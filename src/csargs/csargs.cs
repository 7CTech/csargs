﻿/*
Copyright(c) 2016 7CTech

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;

namespace ncsargs
{
    public class ccsargs
    {
        public static void add(csarg inarg, string name, string longarg, string desc, string shortarg = "0")
        {
            if (shortarg == "0")
            {
                longarg = ("--") + longarg;
                inarg.longarg = longarg;
                inarg.desc = desc;
                inarg.name = name;
            }
            else
            {
                longarg = ("--" + longarg);
                shortarg = ("-" + shortarg);

                inarg.longarg = longarg;
                inarg.shortarg = shortarg;
                inarg.desc = desc;
                inarg.name = name;
            }
        }

        public static bool check(csarg args, string arg)
        {
            if (!(arg.StartsWith("-"))) //Parameters
            {
                return false;
            }
            else
            {
                if (args.shortarg == null)
                {
                    if (args.longarg.Contains(arg))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    if (args.shortarg.Contains(arg) || args.longarg.Contains(arg))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        public static void show(SortedDictionary<string, csarg> args)
        { //WIP
            Console.WriteLine("Usage:");
            for (int i = 0; i < args.Count; i++)
            {
                if (args.ElementAt(i).Value.shortarg == null)
                {
                    Console.WriteLine("[ " + "  " + " ],".PadRight(5) +
                                      args.ElementAt(i).Value.longarg.PadRight((20/* - args.ElementAt(i).Value.longarg.Length*/)) +
                                      args.ElementAt(i).Value.desc);
                }
                else
                {
                    Console.WriteLine("[ " + args.ElementAt(i).Value.shortarg + " ],".PadRight(5) +
                                      args.ElementAt(i).Value.longarg.PadRight((20/* - args.ElementAt(i).Value.longarg.Length*/)) +
                                      args.ElementAt(i).Value.desc);

                }
            }
        }
    }

    public class csarg
    {
        public string name;
        public string longarg;
        public string shortarg;
        public string desc;

    }

}
