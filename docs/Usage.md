# Using CSargs

Note: This assumes you have installed CSargs to your project.

| What it does                             | What to put in your code                 |
| ---------------------------------------- | ---------------------------------------- |
| Add csargs                               | `using csargs = ncsargs.ccsargs;` `using ncsargs;` |
| Initialize a dictionary of all your commands | `SortedDictionary<string, csarg> $(dictionary name) = new SortedDictionary<string, csarg>();` |
| Add an argument                          | `csarg $(argument name) = new csarg();`  |
| Give some details to the argument        | `csargs.add($(argument name), "$(name)", "$(long)", "$description", "$(short)");` |
| Adds the argument to the dictionary      | `commands.Add("$(name)", $(argument name));` |
| Gives usage                              | `csargs.show($(dictionary name));`       |
| Checks what the argument is              | `csargs.check($(argname), "$(string to check)");` |

The below will give you an example of each of the functions in a program:

```c#
using System;
using System.Collections.Generic;
using csargs = ncsargs.ccsargs; //adds csargs
using ncsargs; //adds csargs
namespace Csargsdemo
{
    class Program
    {
        public static int Main(string[] args)
        {
            SortedDictionary<string, csarg> commands = new SortedDictionary<string, csarg>(); //adds dictionary of all arguments
            csarg squarearg = new csarg(); //creates a new csarg called "squarearg"
            csargs.add(squarearg, "square", "square", "square a number", "s"); //adds data to the new argument. The short argument is NOT essential
            commands.Add("square", squarearg); //Adds the argument to the dictionary
            for (int a = 0; a < args.Length; a++)
            {
                if (!(args[a].StartsWith("-"))) //Seperates parameters and arguments. HIGHLY recommended
                {
                    if (a == args.Length - 1) {
                        Console.WriteLine("Error");
                        return 1;
                    }
                    a++;
                }
                if (args.Length < 2)
                {
                    csargs.show(commands); //minimum 2 arguments in our program
                }
                if (csargs.check(squarearg, args[a])) //checks if the given argument is either -s or --square
                {
                    for (int i = 1; i < args.Length;) //ignores the argument, and repeats for each parameter
                    {
                        double userin;
                        double squareresult;
                        if (Double.TryParse(args[i], out userin)) //try to make the string input into a double
                        {
                            squareresult = Math.Pow(userin, 2); //square the double
                            Console.WriteLine(squareresult); //prints out the result
                            i++;
                        }
                        else
                        {
                            Console.WriteLine("Parameter at position " + i + " is not a number");
                            return 1;
                        }
                    }
                    return 0;
                }
                csargs.show(commands); //shows valid arguments
            }
            return 1;
        }
    }
}
```



If you need any help, feel free to contact me on the NuGet page!